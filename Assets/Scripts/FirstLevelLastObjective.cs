﻿using UnityEngine;
using System.Collections;

public class FirstLevelLastObjective : MonoBehaviour {

    GameObject player;
    GUITexture guiTextt;
    public AudioClip voice;
    Color acutallyBlack;
    Color transBlack;

	
    float fadeSpeed = 1f;
    bool playerPositionLocked = false; //IF PLAYER POSITION LOCKED.
    bool faded = false; //IF FADED
    bool fading = false;  //IF FADING
    bool audioPlaying = false; //IF AUDIO IS PLAYING
    bool audioDone = false; //IF AUDIO IS DONE PLAYING.

    Vector3 lockingPosition;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        guiTextt = GetComponent<GUITexture>();
        guiTextt.pixelInset = new Rect(0, 0, Screen.width, Screen.height);
        guiTextt.color = Color.clear;

        acutallyBlack = new Color(0, 0, 0, 255);
        transBlack = new Color(0, 0, 0, 0);
	}
	
	
	void Update () 
    {
	    if(faded)
        {
            if (!playerPositionLocked)
                lockingPosition = player.transform.position;

            player.transform.position = lockingPosition;

            
        }

        if (fading)
        {
            guiTextt.color = Color.Lerp(guiTextt.color, Color.black, fadeSpeed * Time.deltaTime);
            
            if (!audioPlaying)
            {
                GetComponent<AudioSource>().clip = voice;
                GetComponent<AudioSource>().Play();
                GetComponent<AudioSource>().loop = false;
                objectiveManager.FinishObjective("killself", true);
                audioPlaying = true;
            }

            if(audioPlaying)
            {
                if (!GetComponent<AudioSource>().isPlaying)
                {
                    Application.LoadLevel(2);
                    Debug.Log("Finished Playing");
                }
            }
        }
        if (guiTextt.color == Color.black)
            faded = true;
	}


    void OnTriggerEnter2D(Collider2D col)
    {
        if (!fading)
        {
            Debug.Log(GameController.gc.BabyDead);
            if (col.tag == "Player")
                
                if (GameController.gc.BabyDead == true)
                {
                    fading = true;
                    Debug.Log("Fading");
                }
                else
                {
                    Application.LoadLevel(Application.loadedLevel);
                }
        }
    }
}
