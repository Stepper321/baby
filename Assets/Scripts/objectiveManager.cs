﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


    class objectiveManager
    {
        protected static List<Objective> objectives = new List<Objective>();

        public static List<Objective> GetObjectives() { return new List<Objective>(objectives); }

        public static void NewObjective(string objectiveText, string objName)
        {
            if (objectives.Any(x => x.ObjectiveName == objName))
                return;

            objectives.Add(new Objective(objectiveText, objName));  //Add to the list Objective.

            GameController.gc.middleText("New Objective: " + objectiveText);
        }

    public static void NewObjective(string objectiveText, string objName, string objectiveDescription)
    {
        if (objectives.Any(x => x.ObjectiveName == objName))
            return;

        objectives.Add(new Objective(objectiveText, objName));  //Add to the list Objective.

        GameController.gc.descriptionText(objectiveDescription);
        GameController.gc.middleText("New Objective: " + objectiveText);
    }

    public static void FinishObjective(string shortObj, bool FailedObjective)
        {
            var value = objectives.First(x => x.ObjectiveName == shortObj);
            string objectiveText = value.objectiveDescription;

            Debug.Log(value.objectiveDescription);

            string completedTask = value.objectiveDescription;
            objectives.Remove(value);                               //Remove value. (Which doesn't find anything for some reason, so it can't delete anything from the list.)
            if(FailedObjective)
                GameController.gc.middleText("Objective Failed! - " + objectiveText);
            else
                GameController.gc.middleText("Objective Succeeded! - " + objectiveText);
        }
    }
