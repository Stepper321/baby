﻿using UnityEngine;
using System.Collections;
using System;

public class windowsxpexplorerbutton : MonoBehaviour, IClickable {

    [Header("Required")]
    public GameObject blueBorder;
    public Color hoverColour;
    public Color pressColour;
    bool hovering;
    bool clicked;
    [Header("Not needed unless you want it to do something.")]
    public GameObject pageToOpen;
    public GameObject pageToClose;
    [Header("Only for the main Objective.")]
    public bool objectToDelete = false;
    public GameObject deletePrompt;

    public void Clicked()
    {
        clicked = true;
        mousecursor.currentlySelectedFile = gameObject;

        if(mousecursor.doubleClicked)
        {
            if (pageToOpen == null)
                return;

            pageToOpen.SetActive(true);
            if (pageToClose != null)
                pageToClose.SetActive(false);
        }
    }

    public void StartedHovering()
    {
        if (clicked)
            return;

        blueBorder.SetActive(true);
        blueBorder.GetComponent<SpriteRenderer>().color = hoverColour;
        hovering = true;
    }

    public void StoppedClicking()
    {
        
    }

    public void StoppedHovering()
    {
        blueBorder.SetActive(false);
        hovering = false;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(mousecursor.currentlySelectedFile == gameObject)
        {
            blueBorder.SetActive(true);
            blueBorder.GetComponent<SpriteRenderer>().color = pressColour;

            if(Input.GetKeyDown(KeyCode.Delete) || Input.GetMouseButtonDown(1))
            {
                if(objectToDelete)
                {
                    Vector3 positionToIn = transform.position;
                    positionToIn.z += -0.1f;

                    var gameoo = Instantiate(deletePrompt, positionToIn, transform.rotation);
                    Computer.spawnedItems.Add(gameoo);
                }
            }

        }
        else
        {
            clicked = false;

            if (hovering == false)
                blueBorder.SetActive(false);
        }
	}
}
