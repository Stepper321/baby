﻿using UnityEngine;

public interface IDragable
{
    void Dragging(Vector3 mousePosition);
}
