﻿using UnityEngine;
using System.Collections;

public interface IClickable { 
    void Clicked();
    void StoppedClicking();
    void StartedHovering();
    void StoppedHovering();
}
