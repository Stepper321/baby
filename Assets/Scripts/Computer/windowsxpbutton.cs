﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class windowsxpbutton : MonoBehaviour, IClickable {

    public Sprite normalicon =null;
    public Sprite hovericon =null;
    public Sprite pressedicon =null;
    private SpriteRenderer spriteRenderer;
    public GameObject startMenu;
    public static bool startMenuOpen = false;
    bool startMenuJustOpened = false;

    void Start ()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	
	void Update ()
    {
        if (startMenuOpen)
            startMenu.SetActive(true);
        else
            startMenu.SetActive(false);

        if (Input.GetMouseButtonUp(0))
            startMenuJustOpened = false;

	}

    public void Clicked()
    {
        spriteRenderer.sprite = pressedicon;
        startMenuOpen = !startMenuOpen;
        startMenuJustOpened = true;
    }

    public void StoppedClicking()
    {
        spriteRenderer.sprite = normalicon;
    }

    public void StartedHovering()
    {
        spriteRenderer.sprite = hovericon;
    }

    public void StoppedHovering()
    {
        spriteRenderer.sprite = normalicon;
    }
}
