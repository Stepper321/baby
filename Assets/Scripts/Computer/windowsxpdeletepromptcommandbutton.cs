﻿using UnityEngine;
using System.Collections;
using System;

public class windowsxpdeletepromptcommandbutton : MonoBehaviour, IClickable {

    public Sprite normalSprite, hoverSprite;
    public bool pressedYes;

    SpriteRenderer sp = null;
    GameObject computer;

    public void Clicked()
    {
        if (pressedYes)
            StartCoroutine(computer.GetComponent<Computer>().System32End());
        else
            Destroy(transform.parent.gameObject);
    }

    public void StartedHovering()
    {
        sp.sprite = hoverSprite;
    }

    public void StoppedClicking()
    {

    }

    public void StoppedHovering()
    {
        sp.sprite = normalSprite;
    }
    

    void Start ()
    {
        sp = GetComponent<SpriteRenderer>();
        computer = GameObject.Find("Computer");
	}
}
