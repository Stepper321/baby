﻿using UnityEngine;
using System.Collections;

public class windowsstartmenubutton : MonoBehaviour, IClickable {

    public GameObject blueBorder;
    bool playingSong = false;

    public enum WhatToDo
    {
        Nothing,
        Open_Window,
        Play_Sound,
        Play_Song
    }
    public WhatToDo actionToPerform = WhatToDo.Nothing;

    [Header("Not needed unless the action is selected.")]
    public Transform positionToSpawn;
    public GameObject windowToOpen;
    public AudioClip songToPlay;


    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void Clicked()
    {
        if (actionToPerform == WhatToDo.Open_Window)
        {
            if (windowToOpen == null)
                return;

            Vector3 positiontoSpawn = this.positionToSpawn.position;

            var gameoo = Instantiate(windowToOpen, positiontoSpawn, Quaternion.Euler(0, 0, 0));
            Computer.spawnedItems.Add(gameoo);
        }
        else if(actionToPerform == WhatToDo.Play_Song)
        {
            if(!playingSong)
            {
                if (gameObject.GetComponent<AudioSource>() == null)
                    gameObject.AddComponent<AudioSource>();
                var musicPlayer = gameObject.GetComponent<AudioSource>();
                musicPlayer.clip = songToPlay;
                musicPlayer.loop = true;
                musicPlayer.Play();
                playingSong = !playingSong;
            }
        }

        windowsxpbutton.startMenuOpen = false;
    }

    public void StoppedClicking()
    {

    }

    public void StartedHovering()
    {
        blueBorder.SetActive(true);
    }

    public void StoppedHovering()
    {
        blueBorder.SetActive(false);
    }
}
