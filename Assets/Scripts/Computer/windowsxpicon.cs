﻿using UnityEngine;
using System.Collections;
using System;

public class windowsxpicon : MonoBehaviour, IClickable {

    public GameObject border;

    public void Clicked()
    {
        mousecursor.currentlySelectedFile = gameObject;
        if (mousecursor.doubleClicked)
            DoubleClicked();
    }

    void DoubleClicked()
    {
        Debug.Log("I've been double clicked!", gameObject);
    }

    public void StartedHovering()
    {

    }

    public void StoppedClicking()
    {

    }

    public void StoppedHovering()
    {

    }
    
    void Start ()
    {
	
	}
	

	void Update ()
    {
        if (mousecursor.currentlySelectedFile == gameObject)
        {
            border.SetActive(true);
        }
        else
            border.SetActive(false);
	}
}
