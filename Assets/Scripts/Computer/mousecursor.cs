﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class mousecursor : MonoBehaviour
{

    public static GameObject currentlySelectedFile = null;
    public static bool doubleClicked = false;
    public GameObject cursorrayhit;
    public Transform topLeftCorner;
    public Transform topRightCorner;
    public Transform bottomLeftCorner;
    public Transform bottomRightCorner;
    public AudioClip clickySound;
    public float sensitivity = 1.0f;
    AudioSource audioSource;
    IClickable button;
    RaycastHit2D lastButton;

    //Double or single clicking
    float doubleClickDelay = 0.3f;
    float lastClick = 0.0f;
    bool oneClick;

    //Dragging
    IDragable objectToDrag;
    Vector3 lastPosition = Vector3.zero;
    bool currentlyDragging;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    void Update()
    {
        Vector3 wantedPos = Vector3.zero;

        wantedPos.x = transform.position.x + Input.GetAxisRaw("Mouse X") * sensitivity * Time.deltaTime;
        wantedPos.y = transform.position.y + Input.GetAxisRaw("Mouse Y") * sensitivity * Time.deltaTime;
        wantedPos.z = transform.position.z;

        wantedPos.x = Mathf.Clamp(wantedPos.x, topLeftCorner.position.x, topRightCorner.position.x);
        wantedPos.y = Mathf.Clamp(wantedPos.y, bottomRightCorner.position.y, topRightCorner.position.y);

        transform.position = wantedPos;

        RaycastHit2D ray = Physics2D.Raycast(cursorrayhit.transform.position, new Vector2(0, 0));
        if (ray.collider != null)
            button = ray.collider.gameObject.GetComponent<IClickable>();
        else
            button = null;

        if (Input.GetMouseButtonDown(0))
            audioSource.PlayOneShot(clickySound);

        if (button != null) //IF CLICKING
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!oneClick)
                {
                    oneClick = true;

                    lastClick = Time.time;
                    doubleClicked = false;
                    button.Clicked();
                }
                else
                {
                    oneClick = false;
                    doubleClicked = true;
                    button.Clicked();

                }
            }
            else
                button.StartedHovering();
        }
        else //IF DRAGGING
        {
            IDragable dragButton;

            if (ray.collider != null)
                dragButton = ray.collider.gameObject.GetComponent<IDragable>();
            else
                dragButton = null;

            if (dragButton != null)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    currentlyDragging = true;
                    objectToDrag = dragButton;
                }
            }
        }

        if(currentlyDragging)
        {
            if (Input.GetMouseButtonUp(0))
            {
                currentlyDragging = false;
                objectToDrag = null;
                return;
            }

            objectToDrag.Dragging(lastPosition - transform.position);
        }

        if (oneClick)
        {
            if ((Time.time - lastClick) > doubleClickDelay)
                oneClick = false;
        }

        bool lastbuttonissame = (lastButton.collider == ray.collider);
        if (lastButton.collider != null)
        {

            var fuckingButton = lastButton.collider.gameObject.GetComponent<IClickable>();

            if (!lastbuttonissame)
            {
                if (!(fuckingButton == null))
                {
                    fuckingButton.StoppedHovering();
                }
            }
        }



        lastPosition = transform.position;
        lastButton = ray;

    }
}
