﻿using UnityEngine;
using System.Collections;

public class UseableObject : MonoBehaviour {

    public GameObject objectToUse;
    public TextMesh textObject;
    public string object_usertext = "Press E to use this object.";
    public Color wantedColor = new Color(255,255,255,255);
    public float smoothTime = .5f;
    public bool fireOnce = false;
    bool smoothingOut = false;
    IUsable usingObject = null;

    void Start()
    {
        textObject.color = new Color(0, 0, 0, 0);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (usingObject == null)
            usingObject = objectToUse.GetComponent<IUsable>() as IUsable;
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        smoothingOut = false;

        if (textObject == null || objectToUse == null || usingObject == null)
            return;

        textObject.text = object_usertext;

        textObject.color = Color.Lerp(textObject.color, wantedColor, smoothTime * Time.deltaTime);

        if (GameController.gc.playerPressingUse)
        {
            usingObject.OnUse();
            if (fireOnce)
                Destroy(gameObject);
        }

    }

    void OnTriggerExit2D(Collider2D collsion)
    {
        if (textObject == null || objectToUse == null)
            return;

        smoothingOut = true;
    }

    void Update()
    {
        if(smoothingOut)
        {
            textObject.color = Color.Lerp(textObject.color, new Color(0, 0, 0, 0), smoothTime * Time.deltaTime);

            if (textObject.color == new Color(0, 0, 0, 0))
                smoothingOut = false;
        }
    }
}
