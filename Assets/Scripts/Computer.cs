﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Computer : MonoBehaviour, IUsable
{

    bool used = false;

    public GameObject computerCamera;
    public GameObject musicSound;
    public GameObject playerCamera;
    public GameObject black_background;
    public GameObject motherbord_bootscreen;
    public GameObject windowsxp_bootscreen;
    public GameObject windowsxp_welcome;
    public GameObject windowsxp;
    AudioSource audioSource;
    AudioSource computerAudioSource;
    public AudioClip bootSound;
    public AudioClip clickNoises;
    public AudioClip computerStartNoises;
    public AudioClip computerNoises;
    public AudioClip screamyNoises;
    public string levelToLoadAfterEverythingHasBasicallyPassed;
    bool canLoadLevel = false;
    bool soundCheck = false;

    private AsyncOperation async;

    static public List<Object> spawnedItems = new List<Object>();

    void Start()
    {
        objectiveManager.NewObjective("Start up the computer.", "computer1", "It's easy, just press the button.");
        audioSource = GetComponent<AudioSource>();
        computerAudioSource = black_background.GetComponent<AudioSource>();
    }


    void Update()
    {
        if(soundCheck)
        {
            if (!computerAudioSource.isPlaying)
                async.allowSceneActivation = true;
        }
    }

    public void OnUse()
    {
        if (used)
            return;
        Debug.Log("used computer");
        used = true;

        BootComputer();
    }

    void BootComputer()
    {
        musicSound.SetActive(false);
        GameController.gc.FreezePlayer();
        computerCamera.SetActive(true);
        black_background.SetActive(true);
        computerAudioSource.PlayOneShot(computerStartNoises);
        StartCoroutine(StartComputer());
    }

    IEnumerator StartComputer()
    {   
        yield return new WaitForSeconds(5.8f);
        motherbord_bootscreen.SetActive(true);
        //The sound needs to wait 20.146 seconds before playing the next one.
        //The whole start boot sequence sound takes 25.959 seconds.
        yield return new WaitForSeconds(4f);
        motherbord_bootscreen.SetActive(false);
        windowsxp_bootscreen.SetActive(true);
        //sound needs to wait 16.146 seconds before playing the next one.
        yield return new WaitForSeconds(13.146f);
        windowsxp_bootscreen.SetActive(false);
        windowsxp_welcome.SetActive(true);
        objectiveManager.FinishObjective("computer1", false);
        //sound needs to wait 3 seconds before playing the next one.
        yield return new WaitForSeconds(3f);
        computerAudioSource.clip = computerNoises;
        computerAudioSource.Play();
        computerAudioSource.loop = true;
        audioSource.PlayOneShot(bootSound);
        windowsxp_welcome.SetActive(false);
        
        windowsxp.SetActive(true);
        objectiveManager.NewObjective("Delete System32", "computer2", "How the hell would I do that?");
        yield return new WaitForEndOfFrame();
    }

    public IEnumerator System32End()
    {
        windowsxp.SetActive(false);
        foreach(var objectt in spawnedItems)
            DestroyImmediate(objectt);
        
        computerAudioSource.Stop();
        computerAudioSource.clip = screamyNoises;
        objectiveManager.FinishObjective("computer2", false);
        computerAudioSource.loop = false;
        computerAudioSource.Play();
        StartCoroutine(LoadLevel());
        soundCheck = true;
        yield return new WaitForEndOfFrame();
    }

    IEnumerator LoadLevel()
    {
        async = Application.LoadLevelAsync(levelToLoadAfterEverythingHasBasicallyPassed);
        async.allowSceneActivation = false;
        GameController.gc.FreezePlayer(false);
        Debug.Log(async.progress);
        yield return async;
    }
}
