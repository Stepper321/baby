﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class Objective
{
    public string objectiveDescription { get; set; }
    public string ObjectiveName { get; set; }


    public Objective(string objective, string objectiveName)
    {
        objectiveDescription = objective;
        ObjectiveName = objectiveName;
    }
}
