﻿using UnityEngine;
using System.Collections.Generic;

public class LODSCripts : MonoBehaviour {

    float distanceCull = 200;
    GameObject[] Children;
    bool objectsActive = true;

    void Start()
    {
        GetChildren();
    }

    void GetChildren()
    {
        List<GameObject> obj = new List<GameObject>();
        foreach (Transform child in transform)
        {
            obj.Add(child.gameObject);
        }
        Children = obj.ToArray();
    }

	void Update ()
    {
        if (Vector3.Distance(transform.position, Camera.main.transform.position) >= distanceCull)
        {
            if (!objectsActive)
                return;

            foreach (var child in Children)
            {
                child.SetActive(false);
            }

            objectsActive = false;
        }
        else
        {
            if(objectsActive)
                return;

            foreach (var child in Children)
            {
                child.SetActive(true);
            }

            objectsActive = true;
        }
	}
}
