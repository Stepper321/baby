﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    static public MainMenu mainMenu;
    static public GameObject a;
	
	void Awake () {
        a = gameObject;
        mainMenu = a.GetComponent<MainMenu>();
        GameController.gc.ToggleUI(false);
	}

    public void StartNewGame()
    {
        Application.LoadLevel(1);
    }

    public void Quit()
    {
        GameController.gc.Quit();
    }
	

	void Update () {
	
	}
}
