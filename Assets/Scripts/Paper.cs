﻿using UnityEngine;
using System.Collections;

public class Paper : MonoBehaviour
{
    [SerializeField]
    private GameObject pressE = null;
    [SerializeField]


    bool fired = false;

    void Start()
    {

    }


    void Update()
    {

    }

    public void OnTriggerStay(Collider other)
    {
        if (other.tag != "Player")
            return;

        pressE.SetActive(true);

        if(Input.GetKeyDown(KeyCode.E))
        {
            fired = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag != "Player")
            return;

        pressE.SetActive(false);
    }
}
