﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public GameObject point1;
    public GameObject point2;
    public LayerMask layerMask;
    public float speed = 3;
    public float runSpeed = 6f;
    public bool canUse = true;
    public bool canRun = true;
    bool grounded = false;
    bool lastGrounded;
    bool rotating = false;
    Vector2 movement = Vector2.zero;
    Animator anim;
    GameObject groundedTransform;
    GameObject gameController;
    BoxCollider2D groundedCol;
    Rigidbody2D rigidbody_player;

	void Start () 
    {
        movement = transform.position;
        anim = GetComponent<Animator>();
        groundedTransform = GameObject.Find("Player/collision");
        groundedCol = groundedTransform.GetComponent<BoxCollider2D>();
        gameController = GameObject.FindGameObjectWithTag("GameController");
        if (gameController == null)
        {
            Instantiate(Resources.Load("GameController"));
            gameController = GameObject.FindGameObjectWithTag("GameController");
        }
        rigidbody_player = GetComponent<Rigidbody2D>();
        GameController.gc.ToggleUI(true);
	}
	
	
	void Update () 
    {
        if (GameController.gc.playerFreezed)
            return;
        
        movement.x = Input.GetAxis("Horizontal") * speed;
        movement.y = rigidbody_player.velocity.y;

        grounded = Physics2D.OverlapArea(point1.transform.position, point2.transform.position, layerMask);
        
        if (grounded)
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

        //Jumping
        if (grounded && Input.GetButtonDown("Jump"))
            movement.y = 7;
        //Use
        if (Input.GetButtonDown("Use") && canUse)
            GameController.gc.playerPressingUse = true;
        else
            GameController.gc.playerPressingUse = false;

        //Running
        if(Input.GetButton("Run") && canRun)
        {
            anim.SetBool("running", true);
            movement.x = Input.GetAxis("Horizontal") * runSpeed;
        }
        else
        {
            anim.SetBool("running", false);
        }

        //Animation for walking
        if ((Mathf.Abs(Input.GetAxis("Horizontal")) > 0))
            anim.SetBool("moving", true);
        else
            anim.SetBool("moving", false);

        if (Input.GetKeyDown(KeyCode.U))
            GameController.gc.ToggleUI(false);

        Scaling();
        Vector2 desiredMovement = new Vector2(movement.x, movement.y);
        rigidbody_player.velocity = new Vector2(desiredMovement.x, movement.y);

        lastGrounded = grounded;
	}


    void Scaling()
    {
        Vector2 scale = new Vector2(transform.localScale.x, transform.localScale.y);

        if(Input.GetAxis("Horizontal") > 0)
            scale.x = Mathf.Lerp(scale.x, 1, 3.0f);
        if (Input.GetAxis("Horizontal") < 0)
            scale.x = Mathf.Lerp(scale.x, -1, 3.0f);

        transform.localScale = scale;
        return;
    }
    
}
