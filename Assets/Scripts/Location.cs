﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    class Location
    {
        public string levelName = string.Empty;
        public string locationName = string.Empty;

        public Location(string levelName, string locationName)
        {
            this.levelName = levelName;
            this.locationName = locationName;
        }
    }
