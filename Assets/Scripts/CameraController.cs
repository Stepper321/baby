﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public GameObject target;
    public float smoothTime = 2.0f;
    public bool smooth = true;
    public Vector3 offset;

    Vector3 position;
	
	void Start () 
    {
        if (!target)
            GameObject.FindGameObjectWithTag("Player");

        if(target)
        {
            transform.position = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z);
        }
	}
	
	
	void Update () 
    {
        position.x = target.transform.position.x + offset.x;
        position.y = target.transform.position.y + offset.y;

        if(target)
        {
            if (smooth)
                transform.position = new Vector3(
                    Mathf.Lerp(transform.position.x, position.x, smoothTime * Time.deltaTime), Mathf.Lerp(
                    transform.position.y, position.y, smoothTime * Time.deltaTime), transform.position.z);
            else
                transform.position = new Vector3(position.x, position.y, transform.position.z);
        }

        
	}
}
