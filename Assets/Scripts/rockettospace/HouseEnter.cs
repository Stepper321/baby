﻿using UnityEngine;
using System.Collections;
using System;

public class HouseEnter : MonoBehaviour, IUsable {

    public Transform teleportLocation;
    public GameObject cameraGameObject;
    public GameObject houseEnable;
    public GameObject EnableCarScript;

    public void OnUse()
    {
        StartCoroutine(ENTERHOUSE());
    }

    IEnumerator ENTERHOUSE()
    {
        yield return new WaitForSeconds(0.2f);
        Debug.Log("used");
        GameController.gc.FreezePlayer();
        houseEnable.SetActive(true);
        rockettospacecontroller.ro.ChangeCamera(cameraGameObject, null, false);
        objectiveManager.NewObjective("Enter House.", "house1");
        cameraGameObject.SetActive(true);
        if (EnableCarScript)
            EnableCarScript.SetActive(true);
    }


}
