﻿using UnityEngine;
using System.Collections;

public class rockettospacecontroller : MonoBehaviour {

    public static rockettospacecontroller ro = null;
    Transform lastTransform;


    [SerializeField]
    GameObject startingCamera;
    

    GameObject player;
    public GameObject currentCamera;


    void Awake()
    {
        ro = gameObject.GetComponent<rockettospacecontroller>();
        startingCamera.SetActive(true);
        currentCamera = startingCamera;
        player = GameObject.FindGameObjectWithTag("Player");
        objectiveManager.NewObjective("Search wife.", "wife", "Where could she be?");
        GameController.gc.ShowCurrentLocation();
    }
	

	void Update ()
    {
	
	}

    public void ChangeCamera(GameObject camera, Transform toTeleport, bool teleportPlayer)
    {
        if (camera == currentCamera)
            return;

        Debug.Log(currentCamera, currentCamera);
        currentCamera.SetActive(false);
        currentCamera = camera;
        currentCamera.SetActive(true);
        Debug.Log(currentCamera, currentCamera);
        if (teleportPlayer && !(lastTransform == toTeleport))
        {
            Vector3 teleportLocation = toTeleport.position;
            teleportLocation.z = player.transform.position.z;
            player.transform.position = teleportLocation;
            lastTransform = toTeleport;
        }
    }
}