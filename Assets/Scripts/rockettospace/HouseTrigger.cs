﻿using UnityEngine;
using System.Collections;

public class HouseTrigger : MonoBehaviour
{

    [SerializeField]
    GameObject textEnabler;
    [SerializeField]
    GameObject textDisabler;
    [SerializeField]
    GameObject cameraEnabler;
    [SerializeField]
    bool createObjective;
    [SerializeField]
    bool finishObjective;
    [SerializeField]
    bool FireOnce;
    [SerializeField]
    string objectiveName;
    [SerializeField]
    string objectiveshortName;
    [SerializeField]
    string objectiveDescription;
    [SerializeField]
    string objectiveToFinish;

    bool objectiveCreated, objectiveDeleted;

    Animation animation;

    void Start()
    {

    }

    void Actions()
    {
        if (textEnabler)
            textEnabler.SetActive(true);
        if (textDisabler)
            textDisabler.SetActive(false);
        if (cameraEnabler)
            rockettospacecontroller.ro.ChangeCamera(cameraEnabler, null, false);

        if (finishObjective && !objectiveDeleted)
        {
            objectiveManager.FinishObjective(objectiveToFinish, false);
            objectiveDeleted = true;
        }
        if (createObjective && !objectiveCreated)
        {
            objectiveManager.NewObjective(objectiveName, objectiveshortName, objectiveDescription);
            objectiveCreated = true;
        }
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
            return;
        Actions();
        if (FireOnce)
            Destroy(gameObject);
    }
}
