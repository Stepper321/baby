﻿using UnityEngine;
using System.Collections;

public class threedplayer : MonoBehaviour {

    Rigidbody rigid;
    Animator animator;
    public float speed = 4f;
    public float turnSpeed = 7f;

    Vector3 wantedRotation;
    Vector3 movePosition;

    void Start ()
    {
        rigid = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        wantedRotation = transform.rotation.eulerAngles;
    }
	

	void Update ()
    {
        movePosition = transform.forward;
        movePosition = movePosition.normalized;
        movePosition = movePosition * Input.GetAxis("Vertical") * speed * Time.deltaTime;
        movePosition.y = rigid.velocity.y;
        wantedRotation.y += Input.GetAxis("Horizontal") * turnSpeed * Time.deltaTime;

        if(Input.GetAxis("Vertical")>0)
        {
            animator.SetBool("walking", true);
            //animator.speed = 1f;
        }
        else if(Input.GetAxis("Vertical") < 0)
        {
            animator.SetBool("walking", true);
            //animator.speed = -1f;
        }
        else
        {
            animator.SetBool("walking", false);
            //animator.speed = 1f;
        }

        transform.eulerAngles = wantedRotation;
        rigid.velocity = movePosition;
	}
}
