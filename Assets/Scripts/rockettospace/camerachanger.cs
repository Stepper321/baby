﻿using UnityEngine;
using System.Collections;

public class camerachanger : MonoBehaviour
{

    [SerializeField]
    GameObject cameraToChangeTo;
    [SerializeField]
    bool teleportPlayer = false;
    [Header("Required when teleporting.")]
    [SerializeField]
    Transform toTeleport;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (GameController.gc.playerFreezed)
            return;


        if(collision.tag == "Player")
            rockettospacecontroller.ro.ChangeCamera(cameraToChangeTo, toTeleport, teleportPlayer);
    }
}
