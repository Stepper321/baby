﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class BabyRun : MonoBehaviour {

    public GameObject deadBaby;
    GameObject player;
    public float speed = 4.0f;
    public AudioClip finishedMusic;

    Vector3 movement;
	
	void Start () 
    {
        objectiveManager.NewObjective("Kill your baby.", "KillBaby", "Run.");
        player = GameObject.FindGameObjectWithTag("Player");
        movement = transform.position;
        
    }
	
	void Update () 
    {
        movement.x = transform.position.x + speed * Time.deltaTime;
        movement.y = transform.position.y;
        movement.z = transform.position.z;
        speed -= 0.021f * Time.deltaTime;
        if (speed <= 2.92f)
            speed = 2.92f;
        transform.position = movement;
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.tag == "Player")
        {
            Instantiate(deadBaby, transform.position, transform.rotation);
            objectiveManager.FinishObjective("KillBaby", false);
            objectiveManager.NewObjective("Kill yourself.", "killself", "aaaaaaaaaaaaaaaah");
            Camera.main.GetComponent<AudioSource>().clip = finishedMusic;
            Camera.main.GetComponent<AudioSource>().Play();
            Camera.main.GetComponent<AudioSource>().loop = false;
            GameController.gc.BabyDead = true;
            Debug.Log(GameController.gc.BabyDead);
            Destroy(gameObject);
        }
    }
}
