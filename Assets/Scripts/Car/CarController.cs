﻿using UnityEngine;
using System.Collections;

public class CarController : MonoBehaviour {

    public float AntiRoll = 20000.0f;

    [SerializeField]
    float maxTorque = 50f;
    [SerializeField]
    float maxBrakeTorque = 100f;
    [SerializeField]
    float BoostTorque = 2200f;
    [SerializeField]
    Transform centerOfWeight;

    [SerializeField]
    WheelCollider[] wheelColliders = new WheelCollider[4];
    [SerializeField]
    Transform[] tireMeshes = new Transform[4];

    [SerializeField]
    float smoothingTime = 2.0f;

    float startingTorque;

    void MeshPosition()
    {
        for (int i = 0; i < 4; i++)
        {
            Quaternion rot;
            Vector3 pos;
            wheelColliders[i].GetWorldPose(out pos, out rot);
            tireMeshes[i].position = pos;
            tireMeshes[i].rotation = rot;
        }
    }

    void Update()
    {
        MeshPosition();

        float steering = Input.GetAxis("Horizontal");
        float accelerate = Input.GetAxis("Vertical");
        float finalAngle = steering * 45f;

        DoRollBar(wheelColliders[0], wheelColliders[1]);
        DoRollBar(wheelColliders[2], wheelColliders[3]);
        wheelColliders[0].steerAngle = Mathf.Lerp(wheelColliders[0].steerAngle, finalAngle, Time.deltaTime * smoothingTime);
        wheelColliders[1].steerAngle = Mathf.Lerp(wheelColliders[1].steerAngle, finalAngle, Time.deltaTime * smoothingTime);

        if (Input.GetKey(KeyCode.LeftShift))
            maxTorque = BoostTorque;
        else
            maxTorque = startingTorque;

        if(Input.GetKey(KeyCode.Space))
        {
            wheelColliders[0].brakeTorque = maxBrakeTorque;
            wheelColliders[1].brakeTorque = maxBrakeTorque;
            wheelColliders[2].brakeTorque = maxBrakeTorque;
            wheelColliders[3].brakeTorque = maxBrakeTorque;
        }
        else
        {
            wheelColliders[0].brakeTorque = 0;
            wheelColliders[1].brakeTorque = 0;
            wheelColliders[2].brakeTorque = 0;
            wheelColliders[3].brakeTorque = 0;
        }

        for(int i = 0; i < 4; i++)
        {
            wheelColliders[i].motorTorque = accelerate * maxTorque;
        }
    }

    void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = centerOfWeight.localPosition;
        startingTorque = maxTorque;
    }

    void DoRollBar(WheelCollider WheelL, WheelCollider WheelR)
    {
        WheelHit hit;
        float travelL = 1.0f;
        float travelR = 1.0f;

        bool groundedL = WheelL.GetGroundHit(out hit);
        if (groundedL)
            travelL = (-WheelL.transform.InverseTransformPoint(hit.point).y - WheelL.radius) / WheelL.suspensionDistance;

        bool groundedR = WheelR.GetGroundHit(out hit);
        if (groundedR)
            travelR = (-WheelR.transform.InverseTransformPoint(hit.point).y - WheelR.radius) / WheelR.suspensionDistance;

        float antiRollForce = (travelL - travelR) * AntiRoll;

        if (groundedL)
            GetComponent<Rigidbody>().AddForceAtPosition(WheelL.transform.up * -antiRollForce,
                                         WheelL.transform.position);
        if (groundedR)
            GetComponent<Rigidbody>().AddForceAtPosition(WheelR.transform.up * antiRollForce,
                                         WheelR.transform.position);
    }
}
