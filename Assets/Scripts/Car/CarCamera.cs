﻿using UnityEngine;
using System.Collections;

public class CarCamera : MonoBehaviour
{

    public Transform target;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    public float minFOV = 70, maxFOV = 110;
    public float FOVSmoothing = 0.4f;

    private Rigidbody rigidbody;

    float x = 0.0f;
    float y = 0.0f;

    Camera cam;
    Vector3 targetLastPos = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
        cam = GetComponent<Camera>();
        rigidbody = GetComponent<Rigidbody>();

        // Make the rigid body not change rotation
        if (rigidbody != null)
        {
            rigidbody.freezeRotation = true;
        }
    }

    void LateUpdate()
    {
        if (target)
        {
            x += Input.GetAxisRaw("Mouse X") * xSpeed * 0.02f;
            y -= Input.GetAxisRaw("Mouse Y") * ySpeed * 0.02f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);

            distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

            RaycastHit hit;
            if (Physics.Linecast(target.position, transform.position, out hit))
            {
                if (target.tag != "Player")
                    distance -= hit.distance;
            }
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + target.position;

            float carSpeed = (target.transform.position - targetLastPos).magnitude;
            targetLastPos = target.transform.position;
            float wantedFOV = minFOV + (carSpeed * 18);
            wantedFOV = Mathf.Clamp(wantedFOV, minFOV, maxFOV);
<<<<<<< HEAD
=======
            Debug.Log(carSpeed);
>>>>>>> a312150535a349a38a3e5e4e872912012bce32ad
            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, wantedFOV, Time.deltaTime * FOVSmoothing);

            transform.rotation = rotation;
            transform.position = position;
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
