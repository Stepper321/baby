﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public static GameObject gameController;
    public static GameController gc;

    Text objectiveText;
    Text objectiveNotifier;
    Text objectiveDescription;
    GameObject UI;
    Text currentLocation;
   

    public bool BabyDead = false;
    public bool playerPressingUse = false;
    protected bool uiOff = false;
    public bool UIOff { get { return uiOff; } }
    public bool playerFreezed { get; private set; }

    float timeInteveral;
    float timer;
    int LastLevel;

    List<Location> locations = new List<Location>();

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        gameController = gameObject;
        gc = gameController.GetComponent<GameController>();
        objectiveText = GameObject.Find("Objectives").GetComponent<Text>();
        objectiveNotifier = GameObject.Find("Objective notifier").GetComponent<Text>();
        UI = GameObject.FindGameObjectWithTag("UI");
        objectiveDescription = GameObject.Find("objective_description").GetComponent<Text>();
        currentLocation = GameObject.Find("current_location").GetComponent<Text>();
        Locations();
    }

    void Locations()
    {
        locations.Add(new Location("ass", "Busy shopping mall"));
        locations.Add(new Location("mymomsaysno", "Home?"));
        locations.Add(new Location("rockettospace", "To your wife"));
    }

	void Start () 
    {
        transform.position = new Vector3(0, 0, 0);
        ShowCurrentLocation();
	}

    public void ShowCurrentLocation()
    {
        try
        {
            var loc = locations.First(z => z.levelName == Application.loadedLevelName);
            if (loc != null)
                CurrentLocation(loc.locationName);
        }
        catch(System.InvalidOperationException)
        {
           
        }
    }

    public void ToggleUI(bool Active)
    {
        UI.SetActive(Active);
    }

    public void FreezePlayer(bool freeze = true)
    {
        playerFreezed = freeze;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void middleText(string text)
    {
        if (objectiveNotifier.text.Length > 0)
        {
            objectiveNotifier.text = string.Empty;
        }
        objectiveNotifier.text = text;
        StartCoroutine(textTimer(objectiveNotifier, 4f));
        topleftObj();
    }

    public void descriptionText(string text)
    {
        objectiveDescription.text = text;
        StartCoroutine(textTimer(objectiveDescription, 4f));
    }

    public void CurrentLocation(string text)
    {
        currentLocation.text = text;
        StartCoroutine(textTimer(currentLocation, 6));
    }

    IEnumerator textTimer(Text textToEmpty, float time)
    {
        yield return new WaitForSeconds(time);
        textToEmpty.text = string.Empty;
    }

    private void topleftObj()
    {
        objectiveText.GetComponent<Text>().text = string.Empty;

        for(int i = 0; i < objectiveManager.GetObjectives().Count; i++)
        {
            objectiveText.GetComponent<Text>().text += objectiveManager.GetObjectives()[i].objectiveDescription + "\n";
        }
    }
}
