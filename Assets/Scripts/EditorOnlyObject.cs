﻿using UnityEngine;
using System.Collections;

public class EditorOnlyObject : MonoBehaviour {

	
	void Start ()
    {
        GetComponent<MeshRenderer>().enabled = false;
	}
}
